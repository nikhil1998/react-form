import React from "react";

const CheckboxOrRadioGroup = props => (
  <div className="form-group">
    <label className="form-check-label">{props.title} &nbsp;</label>

    {/* <div className="form-check form-check-inline"> */}
    <div className={props.type === "radio" ? "form-check-inline " : "block "}>
      {props.options.map(opt => {
        return (
          <label key={opt} className="form-check-label">
            <input
              value={opt}
              checked={props.selectedOptions.indexOf(opt) > -1}
              onChange={props.controlFunc}
              type={props.type}
            />
            &nbsp;
            {opt}
            &nbsp;&nbsp;
          </label>
        );
      })}
      {/* <div className="invalid-feedback">
        Please select a valid {props.setName}
      </div> */}
    </div>
  </div>
);

export default CheckboxOrRadioGroup;
