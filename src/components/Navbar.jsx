import logo from "../Quantiphi2019.png";
import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-dark bg-secondary text-white mb-4">
        <div style={{ paddingBottom: "1" }}>
          {" "}
          <img src={logo} height="75px" width="75px" alt="" />
        </div>
        <div style={{ marginLeft: "400px" }}>
          <h3> Driveboard Wizard</h3>
        </div>

        <Link to="/">
          <h4 style={{ marginLeft: "350px ", color: "white" }}>Home</h4>
        </Link>
        <Link to="/help">
          <h4 style={{ marginLeft: "1px", color: "white" }}>Help</h4>
        </Link>
      </nav>
    </div>
  );
}

export default Navbar;
