import React from "react";
// import PropTypes from "prop-types";

const SingleInput = props => (
  // <div className="container">
  <div className="form-group ">
    <label className="col-form-label">{props.title}</label>
    <input
      className="form-control mb-2 mr-sm-2"
      //   name={props.name}
      type={props.inputType}
      value={props.content}
      onChange={props.controlFunc}
      placeholder={props.placeholder}
      required
    />
  </div>
  // {/* <div className="invalid-tooltip">Please provide a valid {props.title}.</div> */}
  // </div>
);
// SingleInput.propTypes = {
//   inputType: PropTypes.oneOf(["password", "email", "text","file"]).isRequired,
//   title: PropTypes.string.isRequired,
//   // name: React.PropTypes.string.isRequired,
//   controlFunc: PropTypes.func.isRequired,
//   content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
//   placeholder: PropTypes.string
// };
export default SingleInput;
