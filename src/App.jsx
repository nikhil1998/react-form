import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";
import FormContainer from "./containers/FormContainer";
// import logo from "./Quantiphi2019.png";
// import help from "./helpdeskIcon.png";
import QueryFormContainer from "./containers/QueryFormContainer";
import Error from "./components/Error";
import Navbar from "./components/Navbar";
// import FileUpload from "./components/FileUpload";

// import ParticleJs from "./components/ParticleJs";

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Switch>
          <Route path="/" component={FormContainer} exact />
          <Route path="/help" component={QueryFormContainer} />
          <Route component={Error} />
        </Switch>
      </div>
    );
  }
}

export default App;
