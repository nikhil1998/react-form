import React, { Component } from "react";
import SingleInput from "../components/SingleInput";
import Select from "../components/Select";
import CheckboxOrRadioGroup from "../components/CheckboxOrRadioGroup";
import axios from "axios";
import FileUpload from "../components/FileUpload";

class FormContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {
        emailr: "",
        passwordr: "",
        urlr: "",
        urlGCP: "",
        urls3: ""
      },
      submitAlert: "false",
      hidden: "true",
      projectName: "",
      projectTypes: ["Type-1", "Type-2", "Type-3"],
      projectSelection: "",
      PurposeOptions: ["Infrastructure Provisioning", "ML Operations"],
      PurposeSelection: [],
      urlName: "",
      branchName: "",
      userName: "",
      passWord: "",
      infraOptions: ["GCP", "AWS", "AZURE"],
      infraSelection: [],
      email: "",
      AvailableOptions: [
        "Infrastructure Security check",
        "Monitoring and Reporting",
        "Integration with JIRA",
        "Compliance check"
      ],
      SelectedOptions: [],

      TemplateOptionsGcp: ["Deployment Manager", "Terraform"],
      TemplateSelectedOptionGcp: [],
      TemplateOptionsAws: ["Cloud Formation", "Terraform"],
      TemplateSelectedOptionAws: [],
      SourceProvidersGcp: ["Git", "Cloud Storage", "Local"],
      SourceProviderSelectionGcp: "",
      SourceProvidersAws: ["Git", "S3", "Local"],
      SourceProviderSelectionAws: "",
      TemplateUrlS3: "",
      TemplateUrlGcp: ""
    };
    this.handlePurposeSelection = this.handlePurposeSelection.bind(this);
    this.handleCheckSelectionChange = this.handleCheckSelectionChange.bind(
      this
    );
    this.handleTemplateSelection = this.handleTemplateSelection.bind(this);
    this.handleProviderSelection = this.handleProviderSelection.bind(this);
    this.urlTempNameChange = this.urlTempNameChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
    this.handleProjectNameChange = this.handleProjectNameChange.bind(this);
    this.handleProjectTypeSelect = this.handleProjectTypeSelect.bind(this);
    this.urlNameChange = this.urlNameChange.bind(this);
    this.handleBranchNameChange = this.handleBranchNameChange.bind(this);
    this.handleRepoUserNameChange = this.handleRepoUserNameChange.bind(this);
    this.handleinfraSelectionChange = this.handleinfraSelectionChange.bind(
      this
    );
    this.handleRepoPasswordChange = this.handleRepoPasswordChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);

    this.toggleShow = this.toggleShow.bind(this);
  }

  toggleShow() {
    this.setState({ hidden: !this.state.hidden });
  }
  validEmailRegex = RegExp(
    /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  );
  validUrlRegex = RegExp(
    // "^(https?:\\/\\/)?" +
    //   "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|" +
    //   "((\\d{1,3}\\.){3}\\d{1,3}))" +
    //   "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" +
    //   "(\\?[;&amp;a-z\\d%_.~+=-]*)?" +
    //   "(\\#[-a-z\\d_]*)?$",
    // "i"

    /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/
  );
  regex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;

  handleFormSubmit(e) {
    console.log("in submit");
    // if (!this.canBeSubmitted()) {
    //   e.preventDefault();
    //   return;
    // }
    e.preventDefault();
    this.setState({ submitAlert: "true" });
    const formPayload = {
      projectName: this.state.projectName,
      projectSelection: this.state.projectSelection,
      urlName: this.state.urlName,
      branchName: this.state.branchName,
      userName: this.state.userName,
      passWord: this.state.passWord,
      infraSelection: this.state.infraSelection,
      email: this.state.email,
      SelectedOptions: this.state.SelectedOptions,
      SourceProviderSelectionAws: this.state.SourceProviderSelectionAws,
      SourceProviderSelectionGcp: this.state.SourceProviderSelectionGcp,
      TemplateUrlGcp: this.state.TemplateUrlGcp,
      TemplateUrlAws: this.state.TemplateUrlAws,
      selectedFile: this.state.selectedFile
    };

    console.log("Send this in a POST request:", formPayload);
    console.log(JSON.stringify(formPayload));
    axios
      .post(
        // `https://jsonplaceholder.typicode.com/users`,
        `http://54.210.14.89:8000/serverinput/`,
        // { crossDomain: true },
        { formPayload }
      )
      .then(res => {
        console.log(res);
        console.log(res.data);
      });
    // this.handleClearForm();

    alert(
      "Form have been Submitted successfully ....................... Please wait for few minutes"
    );
  }
  handleClearForm(e) {
    console.log("in clear");
    if (e) {
      e.preventDefault();
    }
    this.setState({
      projectName: "",
      //   projectTypes: ["Type-1", "Type-2", "Type-3"],
      projectSelection: "",
      PurposeSelection: [],
      urlName: "",
      branchName: "",
      userName: "",
      passWord: "",
      infraSelection: [],
      email: "",
      SelectedOptions: [],
      TemplateSelectedOptionGcp: "",
      TemplateSelectedOptionAws: "",
      SourceProviderSelectionAws: "",
      SourceProviderSelectionGcp: "",
      TemplateUrlGcp: "",
      TemplateUrlAws: "",
      selectedFile: null
    });
  }

  handleProjectNameChange(e) {
    this.setState({ projectName: e.target.value });
    console.log(this.state.projectName);
  }
  handleProjectTypeSelect(e) {
    this.setState({ projectSelection: e.target.value });
    console.log(this.state.projectSelection);
  }

  urlNameChange(e) {
    this.setState({ urlName: e.target.value });
    console.log(this.state.urlName);
    console.log(this.state.email);
    let errors = this.state.errors;
    let validUrl = new RegExp(this.validUrlRegex);
    errors.urlr = validUrl.test(e.target.value) ? "" : "Url is not valid!";

    this.setState({ errors, [e.target.name]: e.target.value });
  }
  handleBranchNameChange(e) {
    this.setState({ branchName: e.target.value });
    console.log(this.state.branchName);
  }
  handleRepoUserNameChange(e) {
    this.setState({ userName: e.target.value });
    console.log(this.state.userName);
  }

  handleRepoPasswordChange(e) {
    this.setState({ passWord: e.target.value });
    console.log(this.state.passWord);
    let errors = this.state.errors;
    errors.passwordr =
      e.target.value.length < 8
        ? "Password must be atleast 8 characters long!"
        : "";
    this.setState({ errors, [e.target.name]: e.target.value });
  }
  handleEmailChange(e) {
    console.log(this.state.email);
    let errors = this.state.errors;
    errors.emailr = this.validEmailRegex.test(e.target.value)
      ? ""
      : "Email is not valid!";
    this.setState({ email: e.target.value });
    this.setState({ errors, [e.target.name]: e.target.value });
  }

  handleinfraSelectionChange(e) {
    // console.log(this.state.infraSelection);

    console.log("infra selection", e.target.value);

    this.setState({ infraSelection: e.target.value });
  }

  canBeSubmitted() {
    return (
      this.state.email.length > 0 &&
      this.state.infraSelection.length > 0 &&
      this.state.projectSelection.length > 0 &&
      this.state.projectName.length > 0 &&
      this.state.PurposeSelection.length > 0
    );
  }
  handlePurposeSelection(e) {
    console.log("event in purpose selection", e.target.value);
    this.setState({ PurposeSelection: [e.target.value] });
  }
  handleCheckSelectionChange(e) {
    const newSelection = e.target.value;
    let newSelectionArray;
    if (this.state.SelectedOptions.indexOf(newSelection) > -1) {
      newSelectionArray = this.state.SelectedOptions.filter(
        s => s !== newSelection
      );
    } else {
      newSelectionArray = [...this.state.SelectedOptions, newSelection];
    }

    this.setState({ SelectedOptions: newSelectionArray });
  }
  handleTemplateSelection(e) {
    if (this.state.infraSelection === "GCP") {
      this.setState({ TemplateSelectedOptionGcp: e.target.value });
    } else if (this.state.infraSelection === "AWS") {
      this.setState({ TemplateSelectedOptionAws: e.target.value });
    }
  }
  handleProviderSelection(e) {
    console.log(e.target.value);
    if (this.state.infraSelection === "GCP") {
      this.setState({ SourceProviderSelectionGcp: e.target.value });
      this.setState({ SourceProviderSelectionAws: "" });
    } else if (this.state.infraSelection === "AWS") {
      this.setState({ SourceProviderSelectionAws: e.target.value });
      this.setState({ SourceProviderSelectionGcp: "" });
    }
    // console.log("In provider selection", this.state.SourceProviderSelectionGcp);
    console.log("In provider selection", this.state.SourceProviderSelectionAws);
  }
  validS3Url = /^(http[s]?:\/\/){0,1}(s3-|s3\.)?(.*)\.amazonaws\.com/g;
  validGcpUrl = /(http[s]?:\/\/)?[^\s(["<,>]*\.[^\s[",><]*/gim;
  // http://BUCKET_NAME.storage.googleapis.com/OBJECT_NAME

  // http://storage.googleapis.com/BUCKET_NAME/OBJECT_NAME
  urlTempNameChange(e) {
    if (this.state.infraSelection === "GCP") {
      this.setState({ TemplateUrlGcp: e.target.value });
      let errors = this.state.errors;
      let validUrl = new RegExp(this.validGcpUrl);
      errors.urlGCP = validUrl.test(e.target.value)
        ? ""
        : "GCS Url is not valid!";
      this.setState({ errors, [e.target.name]: e.target.value });
    } else if (this.state.infraSelection === "AWS") {
      this.setState({ TemplateUrlAws: e.target.value });
      let errors = this.state.errors;
      let validUrl = new RegExp(this.validS3Url);
      errors.urls3 = validUrl.test(e.target.value)
        ? ""
        : "S3 Url is not valid!";
      this.setState({ errors, [e.target.name]: e.target.value });
    }
  }
  render() {
    const isEnabled = this.canBeSubmitted();
    return (
      <div className="container bg-light text-dark w-50">
        <form
          className="form-group"
          validate="true"
          onSubmit={this.handleFormSubmit}
          noValidate
        >
          <div
            style={{
              display: "inline-block",
              width: "570px",
              marginRight: "8px"
            }}
          >
            <SingleInput
              inputType={"text"}
              title={"Project Name : "}
              controlFunc={this.handleProjectNameChange}
              content={this.state.projectName}
              placeholder={" Enter project name"}
            />
          </div>
          <i
            className="fa fa-info-circle fa-lg"
            style={{ color: "black" }}
            title="Description about project Name"
          ></i>
          <div
            style={{
              display: "inline-block",
              width: "570px",
              marginRight: "8px"
            }}
          >
            <Select
              title={"Project Type :   "}
              placeholder={"Select Project Type"}
              controlFunc={this.handleProjectTypeSelect}
              options={this.state.projectTypes}
              selectedOption={this.state.projectSelection}
            />
          </div>
          <CheckboxOrRadioGroup
            title={"Purpose:"}
            controlFunc={this.handlePurposeSelection}
            type={"radio"}
            options={this.state.PurposeOptions}
            selectedOptions={this.state.PurposeSelection}
          />

          <CheckboxOrRadioGroup
            title={"Environment :"}
            controlFunc={this.handleinfraSelectionChange}
            type={"radio"}
            options={this.state.infraOptions}
            selectedOptions={this.state.infraSelection}
          />
          {this.state.infraSelection === "GCP" && (
            <div>
              <CheckboxOrRadioGroup
                title={"Template :"}
                controlFunc={this.handleTemplateSelection}
                type={"radio"}
                options={this.state.TemplateOptionsGcp}
                selectedOptions={this.state.TemplateSelectedOptionGcp}
              />
              <CheckboxOrRadioGroup
                title={"Source Provider :"}
                controlFunc={this.handleProviderSelection}
                type={"radio"}
                options={this.state.SourceProvidersGcp}
                selectedOptions={this.state.SourceProviderSelectionGcp}
              />
            </div>
          )}
          {this.state.infraSelection === "AWS" && (
            <div>
              <CheckboxOrRadioGroup
                title={"Template :"}
                controlFunc={this.handleTemplateSelection}
                type={"radio"}
                options={this.state.TemplateOptionsAws}
                selectedOptions={this.state.TemplateSelectedOptionAws}
              />
              <CheckboxOrRadioGroup
                title={"Source Provider :"}
                controlFunc={this.handleProviderSelection}
                type={"radio"}
                options={this.state.SourceProvidersAws}
                selectedOptions={this.state.SourceProviderSelectionAws}
              />
            </div>
          )}
          {this.state.SourceProviderSelectionGcp === "Local" &&
            this.state.infraSelection === "GCP" && <FileUpload />}

          {this.state.SourceProviderSelectionAws === "Local" &&
            this.state.infraSelection === "AWS" && <FileUpload />}

          {this.state.SourceProviderSelectionGcp === "Cloud Storage" &&
            this.state.infraSelection === "GCP" && (
              <div>
                <div
                  style={{
                    display: "inline-block",
                    width: "570px",
                    marginRight: "8px"
                  }}
                >
                  <SingleInput
                    inputType={"text"}
                    title={"GCP Template URL : "}
                    controlFunc={this.urlTempNameChange}
                    content={this.state.TemplateUrlGcp}
                    placeholder={" Enter GCP Template URL"}
                    noValidate
                  />
                </div>
                <i
                  className="fa fa-info-circle fa-lg"
                  title="Description about Template url"
                  style={{ color: "black" }}
                ></i>
                <br />

                {this.state.errors.urlGCP.length > 0 && (
                  <span className="error">{this.state.errors.urlGCP}</span>
                )}
              </div>
            )}

          {this.state.SourceProviderSelectionAws === "S3" &&
            this.state.infraSelection === "AWS" && (
              <div>
                <div
                  style={{
                    display: "inline-block",
                    width: "570px",
                    marginRight: "8px"
                  }}
                >
                  <SingleInput
                    inputType={"text"}
                    title={"AWS Template URL : "}
                    controlFunc={this.urlTempNameChange}
                    // content={this.state.TemplateUrlAws}
                    placeholder={" Enter AWS Template URL"}
                    noValidate
                  />
                </div>
                <i
                  className="fa fa-info-circle fa-lg"
                  title="Description about Template url"
                  style={{ color: "black" }}
                ></i>
                <br />

                {this.state.errors.urls3.length > 0 && (
                  <span className="error">{this.state.errors.urls3}</span>
                )}
              </div>
            )}

          {(this.state.SourceProviderSelectionAws === "Git" &&
            this.state.infraSelection === "AWS" && (
              <div>
                <div
                  style={{
                    display: "inline-block",
                    width: "570px",
                    marginRight: "8px"
                  }}
                >
                  <SingleInput
                    inputType={"text"}
                    title={"Source code URL : "}
                    controlFunc={this.urlNameChange}
                    content={this.state.urlName}
                    placeholder={" Enter Source code URL"}
                    noValidate
                  />
                </div>
                <i
                  className="fa fa-info-circle fa-lg"
                  title="Description about source code url"
                  style={{ color: "black" }}
                ></i>
                <br />
                {this.state.errors.urlr.length > 0 && (
                  <span className="error">{this.state.errors.urlr}</span>
                )}
                <div
                  style={{
                    display: "inline-block",
                    width: "570px",
                    marginRight: "8px"
                  }}
                >
                  <SingleInput
                    inputType={"text"}
                    title={"Source code Branch: "}
                    controlFunc={this.handleBranchNameChange}
                    content={this.state.branchName}
                    placeholder={" Enter Source Code Branch"}
                  />
                </div>
                <i
                  className="fa fa-info-circle fa-lg"
                  title="Description about source code branch"
                  style={{ color: "black" }}
                ></i>
                {/* <div className="col-sm-12 col-form-label"> */}
                {/* <h5 style={{ marginLeft: "140px" }}>Source Repository Credentials</h5> */}
                {/* </div> */}
                <h4 style={{ marginTop: "10px" }}>
                  <center>Source Provider Credentials</center>
                </h4>
                <div
                  style={{
                    display: "inline-block",
                    width: "570px",
                    marginRight: "8px"
                  }}
                >
                  <SingleInput
                    inputType={"text"}
                    title={"Username : "}
                    controlFunc={this.handleRepoUserNameChange}
                    content={this.state.userName}
                    placeholder={" Enter repo username"}
                  />
                </div>
                <i
                  className="fa fa-info-circle fa-lg"
                  title="Description about repo user name"
                  style={{ color: "black" }}
                ></i>
                <div
                  style={{
                    display: "inline-block",
                    width: "570px",
                    marginRight: "4px"
                  }}
                >
                  <SingleInput
                    inputType={this.state.hidden ? "password" : "text"}
                    title={"Password : "}
                    //   name={"name"}
                    controlFunc={this.handleRepoPasswordChange}
                    content={this.state.passWord}
                    placeholder={" Enter repo password"}
                    noValidate
                  />
                </div>
                {/* <div style={{ display: "inline-block", marginLeft: "3px" }}> */}
                {this.state.hidden && (
                  // <button onClick={this.toggleShow} width="2px" height="2px">
                  <span onClick={this.toggleShow}>
                    <i className="fa fa-eye errspan"></i>
                  </span>

                  // </button>
                )}
                {!this.state.hidden && (
                  // <button onClick={this.toggleShow} width="2px" height="2px">
                  <span onClick={this.toggleShow}>
                    <i className="icon-eye-close errspan"></i>
                  </span>
                  // <i className="icon-eye-close"></i>
                  // </button>
                )}
                {/* </div> */}
                {this.state.errors.passwordr.length > 0 && (
                  <span className="error">{this.state.errors.passwordr}</span>
                )}
              </div>
            )) ||
            (this.state.SourceProviderSelectionGcp === "Git" &&
              this.state.infraSelection === "GCP" && (
                <div>
                  <div
                    style={{
                      display: "inline-block",
                      width: "570px",
                      marginRight: "8px"
                    }}
                  >
                    <SingleInput
                      inputType={"text"}
                      title={"Source code URL : "}
                      controlFunc={this.urlNameChange}
                      content={this.state.urlName}
                      placeholder={" Enter Source code URL"}
                      noValidate
                    />
                  </div>
                  <i
                    className="fa fa-info-circle fa-lg"
                    title="Description about source code url"
                    style={{ color: "black" }}
                  ></i>
                  <br />
                  {this.state.errors.urlr.length > 0 && (
                    <span className="error">{this.state.errors.urlr}</span>
                  )}
                  <div
                    style={{
                      display: "inline-block",
                      width: "570px",
                      marginRight: "8px"
                    }}
                  >
                    <SingleInput
                      inputType={"text"}
                      title={"Source code Branch: "}
                      controlFunc={this.handleBranchNameChange}
                      content={this.state.branchName}
                      placeholder={" Enter Source Code Branch"}
                    />
                  </div>
                  <i
                    className="fa fa-info-circle fa-lg"
                    title="Description about source code branch"
                    style={{ color: "black" }}
                  ></i>
                  {/* <div className="col-sm-12 col-form-label"> */}
                  {/* <h5 style={{ marginLeft: "140px" }}>Source Repository Credentials</h5> */}
                  {/* </div> */}
                  <h4 style={{ marginTop: "10px" }}>
                    <center>Source Provider Credentials</center>
                  </h4>
                  <div
                    style={{
                      display: "inline-block",
                      width: "570px",
                      marginRight: "8px"
                    }}
                  >
                    <SingleInput
                      inputType={"text"}
                      title={"Username : "}
                      controlFunc={this.handleRepoUserNameChange}
                      content={this.state.userName}
                      placeholder={" Enter repo username"}
                    />
                  </div>
                  <i
                    className="fa fa-info-circle fa-lg"
                    title="Description about repo user name"
                    style={{ color: "black" }}
                  ></i>
                  <div
                    style={{
                      display: "inline-block",
                      width: "570px",
                      marginRight: "4px"
                    }}
                  >
                    <SingleInput
                      inputType={this.state.hidden ? "password" : "text"}
                      title={"Password : "}
                      //   name={"name"}
                      controlFunc={this.handleRepoPasswordChange}
                      content={this.state.passWord}
                      placeholder={" Enter repo password"}
                      noValidate
                    />
                  </div>
                  {/* <div style={{ display: "inline-block", marginLeft: "3px" }}> */}
                  {this.state.hidden && (
                    // <button onClick={this.toggleShow} width="2px" height="2px">
                    <span onClick={this.toggleShow}>
                      <i className="fa fa-eye errspan"></i>
                    </span>

                    // </button>
                  )}
                  {!this.state.hidden && (
                    // <button onClick={this.toggleShow} width="2px" height="2px">
                    <span onClick={this.toggleShow}>
                      <i className="icon-eye-close errspan"></i>
                    </span>
                    // <i className="icon-eye-close"></i>
                    // </button>
                  )}
                  {/* </div> */}
                  {this.state.errors.passwordr.length > 0 && (
                    <span className="error">{this.state.errors.passwordr}</span>
                  )}
                </div>
              ))}

          <div
            style={{
              display: "inline-block",
              width: "570px",
              marginRight: "8px"
            }}
          >
            <SingleInput
              inputType={"email"}
              title={"QEmail Id : "}
              controlFunc={this.handleEmailChange}
              content={this.state.email}
              placeholder={"firstname.lastname@quantiphi.com"}
              noValidate
            />
          </div>
          <i
            className="fa fa-info-circle fa-lg"
            style={{ color: "black" }}
            title="Please enter quantiphi email-Id"
          ></i>
          {this.state.errors.emailr.length > 0 && (
            <span className="error">{this.state.errors.emailr}</span>
          )}
          <br />
          <CheckboxOrRadioGroup
            title={"Delivery Excellence :"}
            controlFunc={this.handleCheckSelectionChange}
            type={"checkbox"}
            options={this.state.AvailableOptions}
            selectedOptions={this.state.SelectedOptions}
            style={{ display: "block" }}
          />
          <div className="d-flex justify-content-between m-1">
            <button
              type="button"
              onClick={this.handleClearForm}
              className="btn btn-outline-warning"
            >
              Reset
            </button>
            <input
              type="submit"
              disabled={!isEnabled}
              className="btn btn-outline-success"
              value="Submit"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default FormContainer;
